#!/bin/bash
rm -rf cache
helm pull ingress-nginx --untar --destination cache/. --version 4.3.0 --repo "https://kubernetes.github.io/ingress-nginx"
rm -rf bundle/ingress-nginx
helm template ingress-nginx cache/ingress-nginx -f cache/ingress-nginx/values.yaml -f values.yaml --output-dir bundle/. --namespace ingress-nginx
